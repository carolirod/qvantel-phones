# qvantel-phones

exercise for qvantel

## Install the Polymer-CLI

```sh
$ npm install -g polymer-cli
```

## Install dependencies

```sh
$ bower install -F
```

## Serving the Application

```sh
$ polymer serve
```
